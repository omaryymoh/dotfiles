# Replace brew uninstall with brew rmtree
brew() {
  if [ $# -gt 0 ] && [ "$1" == "uninstall" ] ; then
    shift
    command brew rmtree "$@"
  else
    command brew "$@"
  fi
}

# Prevent Terminal.app from importing colorscheme
if [ $TERM_PROGRAM == "iTerm.app" ]; then
  # Import colorscheme from 'wal' asynchronously
  # &   # Run the process in the background.
  # ( ) # Hide shell job control messages.
  (cat ~/.cache/wal/sequences &)
fi

export ANDROID_HOME=$HOME/Library/Android/sdk
export PATH=$PATH:$ANDROID_HOME/tools
export PATH=$PATH:$ANDROID_HOME/tools/bin
export PATH=$PATH:$ANDROID_HOME/platform-tools

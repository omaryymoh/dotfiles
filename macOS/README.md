# macOS

![neofech](neofetch.png)

### Software list:

* [`pywal`](https://github.com/dylanaraps/pywal)
* `iTerm`
* [`Amethyst`](https://github.com/ianyh/Amethyst)
* [`brew`](brew.sh)

# Ubuntu minimal

![neofech](neofetch.png)

### Software list:

* `xorg`
* `i3`
* [`i3 gaps`](https://github.com/Airblader/i3)
* [`i3lock-fancy`](https://github.com/meskarune/i3lock-fancy)
* `urxvt`
* [`polybar`](https://github.com/jaagr/polybar)
* `rofi`
* [`pywal`](https://github.com/dylanaraps/pywal)
* `arandr`
* `firefox`
* `blueman`
* `fonts-font-awesome`
* `feh`
* `gedit`
* `nautilus` 
* `network-manager-gnome`
* `ubuntu-drivers-common`

### Polybar
Edit `monitor` in `~/.config/polybar/config`:
```
monitor = ${env:MONITOR:YOUR_DISPLAY_NAME}
```

### GRUB
Boot into `tty1` instead of `tty7`, edit GRUB file:
```sh
$ sudo nano /etc/default/grub
```
Find following line and edit the value to `text`:
```sh
$ GRUB_CMDLINE_LINUX_DEFAULT="text"
```
Apply changes:
```sh
$ sudo update-grub
```

[colors]
primary = #1c1c1c
secondary = #ebdbb2
line-color = #f00

[paddings]
label = 2

[bar/top]
monitor = ${env:MONITOR:eDP-1}
width = 100%
height = 22
radius = 0
fixed-center = false

background = ${colors.primary}
foreground = ${colors.secondary}

line-size = 1
line-color = ${colors.line-color}

border-size = 0

padding-left = 0
padding-right = 2

module-margin-left = 1
module-margin-right = 3

font-0 = fixed:pixelsize=10;1
font-1 = unifont:fontformat=truetype:size=8:antialias=false;0
font-2 = siji:pixelsize=10;1
font-3 = FontAwesome:pixelsize=10;0

modules-left = i3
modules-center = mpd
modules-right = volume cpu memory battery temperature date

tray-position = right
tray-padding = 2

cursor-click = pointer
cursor-scroll = ns-resize

[module/xwindow]
type = internal/xwindow
label = %title:0:30:...%

[module/i3]
type = internal/i3
format = <label-state> <label-mode>
index-sort = true
wrapping-scroll = false

; Only show workspaces on the same output as the bar
;pin-workspaces = true

label-mode-padding = ${paddings.label}
label-mode-foreground = ${colors.primary}
label-mode-background = ${colors.secondary}

; focused = Active workspace on focused monitor
label-focused = %index%
label-focused-background = ${colors.secondary}
label-focused-foreground = ${colors.primary}
label-focused-padding = ${paddings.label}

; unfocused = Inactive workspace on any monitor
label-unfocused = %index%
label-unfocused-padding = ${paddings.label}

; visible = Active workspace on unfocused monitor
label-visible = %index%
label-visible-background =${colors.secondary}
label-visible-padding = ${paddings.label}

; urgent = Workspace with urgency hint set
label-urgent = %index%
label-urgent-background = ${colors.primary}
label-urgent-padding = ${paddings.label}

[module/cpu]
type = internal/cpu
interval = 2
format-prefix = "CPU "
format-prefix-foreground = ${colors.secondary}
label = %percentage:2%%

[module/memory]
type = internal/memory
interval = 2
format-prefix = "RAM "
format-prefix-foreground = ${colors.secondary}
label = %percentage_used%%

[module/date]
type = internal/date
interval = 5

date =
date-alt = "%d-%m-%Y"

time = %H:%M
time-alt = %H:%M:%S

format-prefix = 
format-prefix-foreground = ${colors.secondary}
label = %date% %time%

[module/volume]
type = internal/volume
format-volume = <label-volume> <bar-volume>
label-volume = 

format-muted-prefix = 
format-muted-foreground = ${colors.secondary}
label-muted = 

bar-volume-width = 10
bar-volume-foreground-0 = #55aa55
bar-volume-foreground-1 = #55aa55
bar-volume-foreground-2 = #55aa55
bar-volume-foreground-3 = #55aa55
bar-volume-foreground-4 = #55aa55
bar-volume-foreground-5 = #f5a70a
bar-volume-foreground-6 = #ff5555
bar-volume-gradient = false
bar-volume-indicator = |
bar-volume-indicator-font = 2
bar-volume-fill = ─
bar-volume-fill-font = 2
bar-volume-empty = ─
bar-volume-empty-font = 2
bar-volume-empty-foreground = ${colors.primary}

[module/battery]
type = internal/battery
battery = BAT1
adapter = ACAD
full-at = 98

format-charging = <animation-charging> <label-charging>

format-discharging = <ramp-capacity> <label-discharging>

format-full-prefix = 
format-full-prefix-foreground = ${colors.secondary}

ramp-capacity-0 = 
ramp-capacity-1 = 
ramp-capacity-2 = 
ramp-capacity-3 = 
ramp-capacity-foreground = ${colors.secondary}

animation-charging-0 = 
animation-charging-foreground = ${colors.secondary}
animation-charging-framerate = 500

[module/temperature]
type = internal/temperature
thermal-zone = 0
warn-temperature = 60

format = <ramp> <label>
format-underline = ${colors.primary}
format-warn = <ramp> <label-warn>
format-warn-underline = ${self.format-underline}

label = %temperature%
label-warn = %temperature%
label-warn-foreground = ${colors.primary}

ramp-0 = 
ramp-1 = 
ramp-2 = 
ramp-foreground = ${colors.primary}

[settings]
screenchange-reload = true

[global/wm]
margin-top = 5
margin-bottom = 5

; vim:ft=dosini

if [ -z "$DISPLAY" -a $XDG_VTNR -eq 1 ]; then
	startx
fi

export PATH="${PATH}:${HOME}/.local/bin/"

# Import colorscheme from 'wal' asynchronously
# &   # Run the process in the background.
# ( ) # Hide shell job control messages.
(cat ~/.cache/wal/sequences &)
